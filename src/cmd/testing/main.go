package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var port = "8080"

func main() {
	log.Fatal(http.ListenAndServe(":"+port, router()))
}

func router() http.Handler {
	r := mux.NewRouter()
	r.Path("/greeting").Methods(http.MethodGet).HandlerFunc(greet)
	return r
}

func greet(w http.ResponseWriter, req *http.Request) {
	_, _ = w.Write([]byte("Hello, world!"))
}
